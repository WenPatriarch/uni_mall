//#ifndef H5
var Vue = require('vue')
//#endif
//#ifdef H5
var Vue = require('vue').default
//#endif
module.exports = { currentPage: null,
    currentPageOptions: {},
    navbarPages: [ "pages/index/index", "pages/cat/cat", "pages/cart/cart", "pages/user/user", "pages/list/list", "pages/search/search", "pages/topic-list/topic-list", "pages/video/video-list", "miaosha/miaosha", "pages/shop/shop", "pt/index/index", "pages/book/index/index", "pages/share/index", "pages/quick-purchase/index/index", "store/m/myshop/myshop", "store/shop-list/shop-list", "integral-mall/index/index", "integral-mall/register/index", "pages/article-detail/article-detail", "pages/article-list/article-list", "pages/order/order" ],
    onLoad: function(e, t) {
		e.route = getCurrentPages()[getCurrentPages().length-1].route;
        this.currentPage = e, this.currentPageOptions = t;
        var o = this;
        if (this.setUserInfo(), this.setWxappImg(), this.setDapp(), this.setParentId(t),
        this.getNavigationBarColor(), this.setDeviceInfo(), this.setPageClasses(), this.setPageNavbar(), 
        this.setBarTitle(), "function" == typeof e.onSelfLoad && e.onSelfLoad(t), o._setFormIdSubmit(), 
        "undefined" != typeof my && "pages/login/login" != e.route && t && (e.options || (e.options = t), 
        Vue.prototype.getApp.core.setStorageSync("last_page_options", t)), "lottery/goods/goods" == e.route && t) {
            if (t.user_id) var n = t.user_id, a = t.id; else if (t.scene && isNaN(t.scene)) {
                var i = decodeURIComponent(t.scene);
                if (i && (i = Vue.prototype.getApp.helper.scene_decode(i)) && i.uid) var n = i.uid, a = i.gid;
            }
            Vue.prototype.getApp.request({
                data: {
                    user_id: n,
                    lottery_id: a
                },
                url: Vue.prototype.getApp.api.lottery.clerk,
                success: function(e) {
                    e.code;
                }
            });
        }
        e.navigatorClick = function(t) {
            o.navigatorClick(t, e);
        }, e.setData({
            __platform: Vue.prototype.getApp.platform
        }), void 0 === e.showToast && (e.showToast = function(e) {
            o.showToast(e);
        }), Vue.prototype.getApp.shareSendCoupon = function(e) {
            o.shareSendCoupon(e);
        }, void 0 === e.setTimeList && (e.setTimeList = function(e) {
            return o.setTimeList(e);
        }), void 0 === e.showLoading && (e.showLoading = function(e) {
            o.showLoading(e);
        }), void 0 === e.hideLoading && (e.hideLoading = function(e) {
            o.hideLoading(e);
        }), void 0 === e.modalConfirm && (e.modalConfirm = function(e) {
            o.modalConfirm(e);
        }), void 0 === e.modalClose && (e.modalClose = function(e) {
            o.modalClose(e);
        }), void 0 === e.modalShow && (e.modalShow = function(e) {
            o.modalShow(e);
        }), void 0 === e.myLogin && (e.myLogin = function() {
            o.myLogin();
        }), void 0 === e.getUserInfo && (e.getUserInfo = function(e) {
            o.getUserInfo(e);
        }), void 0 === e.getPhoneNumber && (e.getPhoneNumber = function(e) {
            o.getPhoneNumber(e);
        }), void 0 === e.bindParent && (e.bindParent = function(e) {
            o.bindParent(e);
        }), void 0 === e.closeCouponBox && (e.closeCouponBox = function(e) {
            o.closeCouponBox(e);
        }), void 0 === e.relevanceSuccess && (e.relevanceSuccess = function(e) {
            o.relevanceSuccess(e);
        }), void 0 === e.relevanceError && (e.relevanceError = function(e) {
            o.relevanceError(e);
        });
    },
    onReady: function(e) {
        this.currentPage = e;
    },
    onShow: function(e) {
        this.currentPage = e, Vue.prototype.getApp.orderPay.init(e, Vue.prototype.getApp), require("../components/quick-navigation/quick-navigation.js").init(this.currentPage);
    },
    onHide: function(e) {
        this.currentPage = e;
    },
    onUnload: function(e) {
        this.currentPage = e;
    },
    onPullDownRefresh: function(e) {
        this.currentPage = e;
    },
    onReachBottom: function(e) {
        this.currentPage = e;
    },
    onShareAppMessage: function(e) {
        this.currentPage = e, setTimeout(function() {
            Vue.prototype.getApp.shareSendCoupon(e);
        }, 1e3);
    },
    imageClick: function(e) {
        console.log("image click", e);
    },
    textClick: function(e) {
        console.log("text click", e);
    },
    tap1: function(e) {
        console.log("tap1", e);
    },
    tap2: function(e) {
        console.log("tap2", e);
    },
    formSubmit_collect: function(e) {
        e.detail.formId;
        console.log("formSubmit_collect--\x3e", e);
    },
    setUserInfo: function() {
        var e = this.currentPage, t = Vue.prototype.getApp.getUser();
        t && e.setData({
            __user_info: t
        });
    },
    setWxappImg: function(e) {
        var t = this.currentPage;
        Vue.prototype.getApp.getConfig(function(e) {
            t.setData({
                __wxapp_img: e.wxapp_img,
                dapp: e.dapp
            });
        });
    },
    setDapp: function(e) {
        var t = this.currentPage;
        Vue.prototype.getApp.getConfig(function(e) {
            e.dapp && t.setData({
                dapp: e.dapp,
                __is_comment: e.dapp ? e.dapp.is_comment : 1,
                __is_sales: e.dapp ? e.dapp.is_sales : 1,
                __is_member_price: e.dapp ? e.dapp.is_member_price : 1,
                __is_share_price: e.dapp ? e.dapp.is_share_price : 1,
                __alipay_mp_config: e.alipay_mp_config
            });
        });
    },
    setParentId: function(e) {
        var t = this.currentPage, o = this;
        if ("/pages/index/index" == t.route && o.setOfficalAccount(), e) {
            var n = 0;
            if (e.user_id) n = e.user_id; else if (e.scene) {
                if (isNaN(e.scene)) {
                    var a = decodeURIComponent(e.scene);
                    a && (a = Vue.prototype.getApp.helper.scene_decode(a)) && a.uid && (n = a.uid);
                } else -1 == t.route.indexOf("clerk") && (n = e.scene);
                o.setOfficalAccount();
            } else if (null !== Vue.prototype.getApp.query) {
                var i = Vue.prototype.getApp.query;
                n = i.uid;
            }
            n && void 0 !== n && (Vue.prototype.getApp.core.setStorageSync(Vue.prototype.getApp.const.PARENT_ID, n), 
            Vue.prototype.getApp.trigger.remove(Vue.prototype.getApp.trigger.events.login, "TRY_TO_BIND_PARENT"), Vue.prototype.getApp.trigger.add(Vue.prototype.getApp.trigger.events.login, "TRY_TO_BIND_PARENT", function() {
                t.bindParent({
                    parent_id: n,
                    condition: 0
                });
            }));
        }
    },
    showToast: function(e) {
        var t = this.currentPage, o = e.duration || 2500, n = e.title || "", a = (e.success, 
        e.fail, e.complete || null);
        t._toast_timer && clearTimeout(t._toast_timer), t.setData({
            _toast: {
                title: n
            }
        }), t._toast_timer = setTimeout(function() {
            var e = t.data._toast;
            e.hide = !0, t.setData({
                _toast: e
            }), "function" == typeof a && a();
        }, o);
    },
    setDeviceInfo: function() {
        var e = this.currentPage, t = [ {
            id: "device_iphone_5",
            model: "iPhone 5"
        }, {
            id: "device_iphone_x",
            model: "iPhone X"
        } ], o = Vue.prototype.getApp.core.getSystemInfoSync();
        if (o.model) {
            o.model.indexOf("iPhone X") >= 0 && (o.model = "iPhone X");
            for (var n in t) t[n].model == o.model && e.setData({
                __device: t[n].id
            });
        }
    },
    setPageNavbar: function() {
        function e(e) {
            var n = !1;
            for (var a in e.navs) {
                var i = e.navs[a].url, r = o.route || o.__route__ || null;
                i = e.navs[a].new_url;
                for (var s in o.options) Vue.prototype.getApp.helper.inArray(s, [ "scene", "user_id", "uid" ]) || (-1 == r.indexOf("?") ? r += "?" : r += "&", 
                r += s + "=" + o.options[s]);
                /*console.log(r), console.log(i),*/ i === "/" + r ? (e.navs[a].active = !0, n = !0) : e.navs[a].active = !1;
            }
            n && (o.setData({
                _navbar: e
            }), t.setPageClasses());
        }
        var t = this, o = this.currentPage, n = Vue.prototype.getApp.core.getStorageSync("_navbar");
        n && e(n);
        var a = !1;
        for (var i in t.navbarPages) if (o.route == t.navbarPages[i]) {
            a = !0;
            break;
        }
        a && Vue.prototype.getApp.request({
            url: Vue.prototype.getApp.api.default.navbar,
            success: function(t) {
                0 == t.code && (e(t.data), Vue.prototype.getApp.core.setStorageSync("_navbar", t.data));
            }
        });
    },
    setPageClasses: function() {
        var e = this.currentPage, t = e.data.__device;
        e.data._navbar && e.data._navbar.navs && e.data._navbar.navs.length > 0 && (t += " show_navbar"), 
        t && e.setData({
            __page_classes: t
        });
    },
    showLoading: function(e) {
        var t = t;
        t.setData({
            _loading: !0
        });
    },
    hideLoading: function(e) {
        this.currentPage.setData({
            _loading: !1
        });
    },
    setTimeList: function(e) {
        function t(e) {
            return e <= 0 && (e = 0), e < 10 ? "0" + e : e;
        }
        var o = "00", n = "00", a = "00", i = 0, r = "", s = "", c = "";
        if (e >= 86400 && (i = parseInt(e / 86400), e %= 86400, r += i + "天", s += i + "天", 
        c += i + "天"), e < 86400) {
            var p = parseInt(e / 3600);
            e %= 3600, r += (a = t(p)) + "小时", s += a + ":", c = i > 0 || p > 0 ? c + a + ":" : "";
        }
        return e < 3600 && (n = t(parseInt(e / 60)), e %= 60, r += n + "分", s += n + ":", 
        c += n + ":"), e < 60 && (r += (o = t(e)) + "秒", s += o, c += o), {
            d: i,
            h: a,
            m: n,
            s: o,
            content: r,
            content_1: s,
            content_ms: c
        };
    },
    setBarTitle: function(e) {
        var t = this.currentPage.route, o = Vue.prototype.getApp.core.getStorageSync(Vue.prototype.getApp.const.WX_BAR_TITLE);
        for (var n in o) o[n].url === t && Vue.prototype.getApp.core.setNavigationBarTitle({
            title: o[n].title
        });
    },
    getNavigationBarColor: function() {
        var e = Vue.prototype.getApp, t = this;
        e.request({
            url: e.api.default.navigation_bar_color,
            success: function(o) {
                0 == o.code && (e.core.setStorageSync(Vue.prototype.getApp.const.NAVIGATION_BAR_COLOR, o.data), 
                t.setNavigationBarColor(), e.navigateBarColorCall && "function" == typeof e.navigateBarColorCall && e.navigateBarColorCall(o));
            }
        });
    },
    setNavigationBarColor: function() {
        var e = this.currentPage, t = Vue.prototype.getApp.core.getStorageSync(Vue.prototype.getApp.const.NAVIGATION_BAR_COLOR);
        t && (Vue.prototype.getApp.core.setNavigationBarColor(t), e.setData({
            _navigation_bar_color: t
        })), Vue.prototype.getApp.navigateBarColorCall = function(t) {
            Vue.prototype.getApp.core.setNavigationBarColor(t.data), e.setData({
                _navigation_bar_color: t.data
            });
        };
    },
    navigatorClick: function(e, t) {
        var o = e.currentTarget.dataset.open_type;
        if ("redirect" == o) return !0;
        if ("wxapp" != o) {
            if ("tel" == o) {
                var n = e.currentTarget.dataset.tel;
                Vue.prototype.getApp.core.makePhoneCall({
                    phoneNumber: n
                });
            }
            return !1;
        }
    },
    shareSendCoupon: function(e) {
        var t = Vue.prototype.getApp;
        t.core.showLoading({
            mask: !0
        }), e.hideGetCoupon || (e.hideGetCoupon = function(o) {
            var n = o.currentTarget.dataset.url || !1;
            e.setData({
                get_coupon_list: null
            }), n && t.core.navigateTo({
                url: n
            });
        }), t.request({
            url: t.api.coupon.share_send,
            success: function(t) {
                0 == t.code && e.setData({
                    get_coupon_list: t.data.list
                });
            },
            complete: function() {
                t.core.hideLoading();
            }
        });
    },
    bindParent: function(e) {
        var t = Vue.prototype.getApp;
        if ("undefined" != e.parent_id && 0 != e.parent_id) {
            var o = t.getUser();
            t.core.getStorageSync(t.const.SHARE_SETTING).level > 0 && 0 != e.parent_id && t.request({
                url: t.api.share.bind_parent,
                data: {
                    parent_id: e.parent_id,
                    condition: e.condition
                },
                success: function(e) {
                    0 == e.code && (o.parent = e.data, t.setUser(o));
                }
            });
        }
    },
    _setFormIdSubmit: function(e) {
        var t = this.currentPage;
        t._formIdSubmit || (t._formIdSubmit = function(e) {
            var o = e.currentTarget.dataset, n = e.detail.formId, a = o.bind || null, i = o.type || null, r = o.url || null, s = o.appId || null, c = Vue.prototype.getApp.core.getStorageSync(Vue.prototype.getApp.const.FORM_ID_LIST);
            c && c.length || (c = []);
            var p = [];
            for (var g in c) p.push(c[g].form_id);
            switch (console.log("form_id"), "the formId is a mock one" === n || Vue.prototype.getApp.helper.inArray(n, p) || (c.push({
                time: Vue.prototype.getApp.helper.time(),
                form_id: n
            }), Vue.prototype.getApp.core.setStorageSync(Vue.prototype.getApp.const.FORM_ID_LIST, c)), t[a] && "function" == typeof t[a] && t[a](e), 
            i) {
              case "navigate":
                r && Vue.prototype.getApp.core.navigateTo({
                    url: r
                });
                break;

              case "redirect":
                r && Vue.prototype.getApp.core.redirectTo({
                    url: r
                });
                break;

              case "switchTab":
                r && Vue.prototype.getApp.core.switchTab({
                    url: r
                });
                break;

              case "reLaunch":
                r && Vue.prototype.getApp.core.reLaunch({
                    url: r
                });
                break;

              case "navigateBack":
                r && Vue.prototype.getApp.core.navigateBack({
                    url: r
                });
                break;

              case "wxapp":
                s && Vue.prototype.getApp.core.navigateToMiniProgram({
                    url: r,
                    appId: s,
                    path: o.path || ""
                });
            }
        });
    },
    modalClose: function(e) {
        this.currentPage.setData({
            modal_show: !1
        }), console.log("你点击了关闭按钮");
    },
    modalConfirm: function(e) {
        this.currentPage.setData({
            modal_show: !1
        }), console.log("你点击了确定按钮");
    },
    modalShow: function(e) {
        this.currentPage.setData({
            modal_show: !0
        }), console.log("点击会弹出弹框");
    },
    getUserInfo: function(e) {
        var t = this;
        "getUserInfo:ok" == e.detail.errMsg && Vue.prototype.getApp.core.login({
            success: function(o) {
                var n = o.code;
                t.unionLogin({
                    code: n,
                    user_info: e.detail.rawData,
                    encrypted_data: e.detail.encryptedData,
                    iv: e.detail.iv,
                    signature: e.detail.signature
                });
            },
            fail: function(e) {}
        });
    },
    myLogin: function() {
        var e = this;
        "my" === Vue.prototype.getApp.platform && (console.log(Vue.prototype.getApp.login_complete), Vue.prototype.getApp.login_complete || (Vue.prototype.getApp.login_complete = !0, 
        my.getAuthCode({
            scopes: "auth_user",
            success: function(t) {
                e.unionLogin({
                    code: t.authCode
                });
            },
            fail: function(e) {
                Vue.prototype.getApp.login_complete = !1, Vue.prototype.getApp.core.redirectTo({
                    url: "/pages/index/index"
                });
            }
        })));
    },
    unionLogin: function(e) {
        var t = this.currentPage, o = this;
        Vue.prototype.getApp.core.showLoading({
            title: "正在登录",
            mask: !0
        }), Vue.prototype.getApp.request({
            url: Vue.prototype.getApp.api.passport.login,
            method: "POST",
            data: e,
            success: function(e) {
                if (0 == e.code) {
                    t.setData({
                        __user_info: e.data
                    }), Vue.prototype.getApp.setUser(e.data), Vue.prototype.getApp.core.setStorageSync(Vue.prototype.getApp.const.ACCESS_TOKEN, e.data.access_token), 
                    Vue.prototype.getApp.trigger.run(Vue.prototype.getApp.trigger.events.login);
                    var n = Vue.prototype.getApp.core.getStorageSync(Vue.prototype.getApp.const.DAPP);
                    e.data.binding || !n.option.phone_auth || n.option.phone_auth && 0 == n.option.phone_auth ? o.loadRoute() : ("undefined" == typeof wx && o.loadRoute(), 
                    o.setPhone()), o.setUserInfoShowFalse();
                } else Vue.prototype.getApp.login_complete = !1, Vue.prototype.getApp.core.showModal({
                    title: "提示",
                    content: e.msg,
                    showCancel: !1
                });
            },
            fail: function() {
                Vue.prototype.getApp.login_complete = !1;
            },
            complete: function() {
                Vue.prototype.getApp.core.hideLoading();
            }
        });
    },
    getPhoneNumber: function(e) {
        var t = this.currentPage, o = this;
        "getPhoneNumber:fail user deny" == e.detail.errMsg ? Vue.prototype.getApp.core.showModal({
            title: "提示",
            showCancel: !1,
            content: "未授权"
        }) : (Vue.prototype.getApp.core.showLoading({
            title: "授权中"
        }), Vue.prototype.getApp.core.login({
            success: function(n) {
                if (n.code) {
                    var a = n.code;
                    Vue.prototype.getApp.request({
                        url: Vue.prototype.getApp.api.user.user_binding,
                        method: "POST",
                        data: {
                            iv: e.detail.iv,
                            encryptedData: e.detail.encryptedData,
                            code: a
                        },
                        success: function(e) {
                            if (0 == e.code) {
                                var n = t.data.__user_info;
                                n.binding = e.data.dataObj, Vue.prototype.getApp.setUser(n), t.setData({
                                    PhoneNumber: e.data.dataObj,
                                    __user_info: n,
                                    binding: !0,
                                    binding_num: e.data.dataObj
                                }), o.loadRoute();
                            } else Vue.prototype.getApp.core.showToast({
                                title: "授权失败,请重试"
                            });
                        },
                        complete: function(e) {
                            Vue.prototype.getApp.core.hideLoading();
                        }
                    });
                } else Vue.prototype.getApp.core.showToast({
                    title: "获取用户登录态失败！" + n.errMsg
                });
            }
        }));
    },
    setUserInfoShow: function() {
        var e = this.currentPage;
        "wx" == Vue.prototype.getApp.platform ? e.setData({
            user_info_show: !0
        }) : this.myLogin();
    },
    setPhone: function() {
        var e = this.currentPage;
        "undefined" == typeof my && e.setData({
            user_bind_show: !0
        });
    },
    setUserInfoShowFalse: function() {
        this.currentPage.setData({
            user_info_show: !1
        });
    },
    closeCouponBox: function(e) {
        this.currentPage.setData({
            get_coupon_list: ""
        });
    },
    relevanceSuccess: function(e) {
        console.log(e);
    },
    relevanceError: function(e) {
        console.log(e);
    },
    setOfficalAccount: function(e) {
        this.currentPage.setData({
            __is_offical_account: !0
        });
    },
    loadRoute: function() {
        var e = this.currentPage, t = this;
        "pages/index/index" == e.route || Vue.prototype.getApp.core.redirectTo({
            url: "/" + e.route + "?" + Vue.prototype.getApp.helper.objectToUrlParams(e.options)
        }), t.setUserInfoShowFalse();
    }
};