//#ifndef H5
var Vue = require('vue')
//#endif
//#ifdef H5
var Vue = require('vue').default
//#endif
module.exports = function() {
    var t = Vue.prototype.getApp.core.getStorageSync(Vue.prototype.getApp.const.USER_INFO);
    return t || "";
};