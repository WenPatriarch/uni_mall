//#ifndef H5
var Vue = require('vue')
//#endif
//#ifdef H5
var Vue = require('vue').default
//#endif
module.exports = function(t) {
    Vue.prototype.getApp.api;
    var e = Vue.prototype.getApp.core, g = Vue.prototype.getApp;
    if (t && "function" == typeof t) {
        var n = e.getStorageSync(g.const.DAPP_CONFIG);
        n && t(n), g.config ? n = g.config : (Vue.prototype.getApp.trigger.add(Vue.prototype.getApp.trigger.events.callConfig, "call", function(e) {
            t(e);
        }), Vue.prototype.getApp.configReadyCall && "function" == typeof Vue.prototype.getApp.configReadyCall || (Vue.prototype.getApp.configReadyCall = function(t) {
            Vue.prototype.getApp.trigger.run(Vue.prototype.getApp.trigger.events.callConfig, function() {}, t);
        }));
    }
};