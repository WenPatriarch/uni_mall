//#ifndef H5
var Vue = require('vue')
//#endif
//#ifdef H5
var Vue = require('vue').default
//#endif
function e(e) {
    t.onShowData || (t.onShowData = {}), t.onShowData.scene = e;
}

var t = Vue.prototype.getApp, o = {
    init: function(o, a) {
        var r = this, p = Vue.prototype.getApp.api;
        r.page = o, t = a;
        var s = Vue.prototype.getApp.core.getStorageSync(Vue.prototype.getApp.const.PARENT_ID);
        s || (s = 0), r.page.orderPay = function(o) {
            function a(o, a, r) {
								o.pay_type = "WECHAT_PAY",t.request({
                    url: a,
                    data: o,
                    complete: function() {
                        Vue.prototype.getApp.core.hideLoading();
                    },
                    success: function(t) {
                        0 == t.code && (e("pay"), Vue.prototype.getApp.core.requestPayment({
                            _res: t,
                            timeStamp: t.data.timeStamp,
                            nonceStr: t.data.nonceStr,
                            package: t.data.package,
                            signType: t.data.signType,
                            paySign: t.data.paySign,
                            success: function(e) {},
                            fail: function(e) {},
                            complete: function(e) {
                                "requestPayment:fail" != e.errMsg && "requestPayment:fail cancel" != e.errMsg ? Vue.prototype.getApp.core.redirectTo({
                                    url: "/" + r + "?status=1"
                                }) : Vue.prototype.getApp.core.showModal({
                                    title: "提示",
                                    content: "订单尚未支付",
                                    showCancel: !1,
                                    confirmText: "确认",
                                    success: function(e) {
                                        e.confirm && Vue.prototype.getApp.core.redirectTo({
                                            url: "/" + r + "?status=0"
                                        });
                                    }
                                });
                            }
                        })), 1 == t.code && Vue.prototype.getApp.core.showToast({
                            title: t.msg,
                            image: "/static/images/icon-warning.png"
                        });
                    }
                });
            }
            function i(e, o, a) {
                e.pay_type = "BALANCE_PAY";
                var r = Vue.prototype.getApp.getUser();
                Vue.prototype.getApp.core.showModal({
                    title: "当前账户余额：" + r.money,
                    content: "是否使用余额",
                    success: function(r) {
                        r.confirm && (Vue.prototype.getApp.core.showLoading({
                            title: "正在提交",
                            mask: !0
                        }), t.request({
                            url: o,
                            data: e,
                            complete: function() {
                                Vue.prototype.getApp.core.hideLoading();
                            },
                            success: function(e) {
                                0 == e.code && Vue.prototype.getApp.core.redirectTo({
                                    url: "/" + a + "?status=1"
                                }), 1 == e.code && Vue.prototype.getApp.core.showModal({
                                    title: "提示",
                                    content: e.msg,
                                    showCancel: !1
                                });
                            }
                        }));
                    }
                });
            }
            var c = o.currentTarget.dataset.index, n = r.page.data.order_list[c], d = new Array();
            if (void 0 !== r.page.data.pay_type_list) d = r.page.data.pay_type_list; else if (void 0 !== n.pay_type_list) d = n.pay_type_list; else if (void 0 !== n.goods_list[0].pay_type_list) d = n.goods_list[0].pay_type_list; else {
                var g = {};
                g.payment = 0, d.push(g);
            }
            var u = getCurrentPages(), l = u[u.length - 1].route, _ = {};
            if (-1 != l.indexOf("pt")) A = p.group.pay_data, _.order_id = n.order_id; else if (-1 != l.indexOf("miaosha")) A = p.miaosha.pay_data, 
            _.order_id = n.order_id; else if (-1 != l.indexOf("book")) A = p.book.order_pay, 
            _.id = n.id; else {
                var A = p.order.pay_data;
                _.order_id = n.order_id;
            }
            _.parent_id = s, _.condition = 2, 1 == d.length ? (Vue.prototype.getApp.core.showLoading({
                title: "正在提交",
                mask: !0
            }), 0 == d[0].payment && a(_, A, l), 3 == d[0].payment && i(_, A, l)) : Vue.prototype.getApp.core.showModal({
                title: "提示",
                content: "选择支付方式",
                cancelText: "余额支付",
                confirmText: "线上支付",
                success: function(e) {
                    e.confirm ? (Vue.prototype.getApp.core.showLoading({
                        title: "正在提交",
                        mask: !0
                    }), a(_, A, l)) : e.cancel && i(_, A, l);
                }
            });
        }, r.page.order_submit = function(a, i) {
            function c() {
                Vue.prototype.getApp.core.showLoading({
                    title: "正在提交",
                    mask: !0
                }), t.request({
                    url: n,
                    method: "post",
                    data: a,
                    success: function(p) {
                        if (0 == p.code) {
                            var c = function() {
                                t.request({
                                    url: d,
                                    data: {
                                        order_id: n,
                                        order_id_list: u,
                                        pay_type: l,
                                        form_id: a.formId,
                                        parent_user_id: s,
                                        condition: 2
                                    },
                                    success: function(e) {
                                        if (0 != e.code) return Vue.prototype.getApp.core.hideLoading(), void Vue.prototype.getApp.core.showModal({
                                            title: "提示",
                                            content: e.msg,
                                            showCancel: !1,
                                            confirmText: "确认",
                                            success: function(e) {
                                                e.confirm && Vue.prototype.getApp.core.redirectTo({
                                                    url: g + "?status=0"
                                                });
                                            }
                                        });
                                        setTimeout(function() {
                                            Vue.prototype.getApp.core.hideLoading();
                                        }, 1e3), "pt" == i ? "ONLY_BUY" == r.page.data.type ? Vue.prototype.getApp.core.redirectTo({
                                            url: g + "?status=2"
                                        }) : Vue.prototype.getApp.core.redirectTo({
                                            url: "/pages/pt/group/details?oid=" + n
                                        }) : void 0 !== r.page.data.goods_card_list && r.page.data.goods_card_list.length > 0 && 2 != a.payment ? r.page.setData({
                                            show_card: !0
                                        }) : Vue.prototype.getApp.core.redirectTo({
                                            url: g + "?status=-1"
                                        });
                                    }
                                });
                            };
                            if (Vue.prototype.getApp.page.bindParent({
                                parent_id: s,
                                condition: 1
                            }), void 0 != p.data.p_price && 0 === p.data.p_price) return "step" == i ? Vue.prototype.getApp.core.showToast({
                                title: "兑换成功"
                            }) : Vue.prototype.getApp.core.showToast({
                                title: "提交成功"
                            }), void setTimeout(function() {
                                Vue.prototype.getApp.core.redirectTo({
                                    url: "/pages/order/order?status=1"
                                });
                            }, 2e3);
                            setTimeout(function() {
                                r.page.setData({
                                    options: {}
                                });
                            }, 1);
                            var n = p.data.order_id || "", u = p.data.order_id_list ? JSON.stringify(p.data.order_id_list) : "", l = "";
														var token = Vue.prototype.getApp.core.getStorageSync(Vue.prototype.getApp.const.ACCESS_TOKEN)
														var PAY_URL = d+'&order_id='+n+'&order_id_list='+u+'&pay_type=ALIPAY_WAP_PAY&parent_user_id='+s+'&condition=2'+'&access_token='+token+'&_version='+Vue.prototype.getApp._version+'&_plaform='+Vue.prototype.getApp.platform;
                            0 == a.payment ? (
														// #ifdef H5
														androidjs.openChrome(PAY_URL),Vue.prototype.getApp.core.redirectTo({
                              url: g + "?status=0"
                            })
														// #endif
														// #ifdef MP-WEIXIN
														t.request({
                                url: d,
                                data: {
                                    order_id: n,
                                    order_id_list: u,
																		pay_type: "WECHAT_PAY",
                                    parent_user_id: s,
                                    condition: 2
                                },
                                success: function(t) {
                                    if (0 != t.code) {
                                        if (1 == t.code) return Vue.prototype.getApp.core.hideLoading(), void Vue.prototype.getApp.core.showToast({
                                            title: t.msg,
                                            image: "/static/images/icon-warning.png"
                                        });
                                    } else {
                                        setTimeout(function() {
                                            Vue.prototype.getApp.core.hideLoading();
                                        }, 1e3), e("pay"), t.data && 0 == t.data.price ? void 0 !== r.page.data.goods_card_list && r.page.data.goods_card_list.length > 0 ? r.page.setData({
                                            show_card: !0
                                        }) : Vue.prototype.getApp.core.redirectTo({
                                            url: g + "?status=1"
                                        }) : Vue.prototype.getApp.core.requestPayment({
                                            _res: t,
                                            timeStamp: t.data.timeStamp,
                                            nonceStr: t.data.nonceStr,
                                            package: t.data.package,
                                            signType: t.data.signType,
                                            paySign: t.data.paySign,
                                            success: function(e) {},
                                            fail: function(e) {},
                                            complete: function(e) {
                                                "requestPayment:fail" != e.errMsg && "requestPayment:fail cancel" != e.errMsg ? "requestPayment:ok" != e.errMsg || (void 0 !== r.page.data.goods_card_list && r.page.data.goods_card_list.length > 0 ? r.page.setData({
                                                    show_card: !0
                                                }) : "pt" == i ? "ONLY_BUY" == r.page.data.type ? Vue.prototype.getApp.core.redirectTo({
                                                    url: g + "?status=2"
                                                }) : Vue.prototype.getApp.core.redirectTo({
                                                    url: "/pages/pt/group/details?oid=" + n
                                                }) : Vue.prototype.getApp.core.redirectTo({
                                                    url: g + "?status=1"
                                                })) : Vue.prototype.getApp.core.showModal({
                                                    title: "提示",
                                                    content: "订单尚未支付",
                                                    showCancel: !1,
                                                    confirmText: "确认",
                                                    success: function(e) {
                                                        e.confirm && Vue.prototype.getApp.core.redirectTo({
                                                            url: g + "?status=0"
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                        var a = Vue.prototype.getApp.core.getStorageSync(Vue.prototype.getApp.const.QUICK_LIST);
                                        if (a) {
                                            for (var p = a.length, s = 0; s < p; s++) for (var c = a[s].goods, d = c.length, u = 0; u < d; u++) c[u].num = 0;
                                            Vue.prototype.getApp.core.setStorageSync(Vue.prototype.getApp.const.QUICK_LISTS, a);
                                            for (var l = Vue.prototype.getApp.core.getStorageSync(Vue.prototype.getApp.const.CARGOODS), p = l.length, s = 0; s < p; s++) l[s].num = 0, 
                                            l[s].goods_price = 0, o.setData({
                                                carGoods: l
                                            });
                                            Vue.prototype.getApp.core.setStorageSync(Vue.prototype.getApp.const.CARGOODS, l);
                                            var _ = Vue.prototype.getApp.core.getStorageSync(Vue.prototype.getApp.const.TOTAL);
                                            _ && (_.total_num = 0, _.total_price = 0, Vue.prototype.getApp.core.setStorageSync(Vue.prototype.getApp.const.TOTAL, _));
                                            Vue.prototype.getApp.core.getStorageSync(Vue.prototype.getApp.const.CHECK_NUM);
                                            0, Vue.prototype.getApp.core.setStorageSync(Vue.prototype.getApp.const.CHECK_NUM, 0);
                                            for (var A = Vue.prototype.getApp.core.getStorageSync(Vue.prototype.getApp.const.QUICK_HOT_GOODS_LISTS), p = A.length, s = 0; s < p; s++) A[s].num = 0, 
                                            o.setData({
                                                quick_hot_goods_lists: A
                                            });
                                            Vue.prototype.getApp.core.setStorageSync(Vue.prototype.getApp.const.QUICK_HOT_GOODS_LISTS, A);
                                        }
                                    }
                                }
                            }) 
														// #endif
														): 2 == a.payment ? (l = "HUODAO_PAY", c()) : 3 == a.payment && (l = "BALANCE_PAY", 
                            c());
                        }
                        1 == p.code && (Vue.prototype.getApp.core.hideLoading(), "活力币不足" == p.msg && r.page.data.dapp.option.step.currency_name ? Vue.prototype.getApp.core.showToast({
                            icon: "none",
                            title: r.page.data.dapp.option.step.currency_name + "不足"
                        }) : Vue.prototype.getApp.core.showToast({
                            icon: "none",
                            title: p.msg
                        }));
                    }
                });
            }
            var n = p.order.submit, d = p.order.pay_data, g = "/pages/order/order";
            if ("pt" == i ? (n = p.group.submit, d = p.group.pay_data, g = "/pages/pt/order/order") : "ms" == i ? (n = p.miaosha.submit, 
            d = p.miaosha.pay_data, g = "/pages/miaosha/order/order") : "pond" == i ? (n = p.pond.submit, 
            d = p.order.pay_data, g = "/pages/order/order") : "scratch" == i ? (n = p.scratch.submit, 
            d = p.order.pay_data, g = "/pages/order/order") : "lottery" == i ? (n = p.lottery.submit, 
            d = p.order.pay_data, g = "/pages/order/order") : "step" == i ? (n = p.step.submit, 
            d = p.order.pay_data, g = "/pages/order/order") : "s" == i && (n = p.order.new_submit, 
            d = p.order.pay_data, g = "/pages/order/order"), 3 == a.payment) {
                var u = Vue.prototype.getApp.getUser();
                Vue.prototype.getApp.core.showModal({
                    title: "当前账户余额：" + u.money,
                    content: "是否确定使用余额支付",
                    success: function(e) {
                        e.confirm && c();
                    }
                });
            } else c();
        };
    }
};

module.exports = o;