import Vue from 'vue'
import App from './App'
import compCommon from './components/common/common.vue'
import compHeader from './components/header/header.vue'
import compQuickNavigation from './components/quick-navigation/quick-navigation.vue'
import compFooter from './components/footer/footer.vue'
import compCopyright from './components/copyright/copyright.vue'
import wxParse from './components/mpvue-wxparse/src/wxParse.vue'
Vue.component('comp-common',compCommon)
Vue.component('comp-header',compHeader)
Vue.component('comp-quick-navigation',compQuickNavigation)
Vue.component('comp-footer',compFooter)
Vue.component('comp-copyright',compCopyright)
Vue.component('wx-parse',wxParse)
Vue.config.productionTip = false

var e = null;

"undefined" != typeof wx && (e = "wx"), "undefined" != typeof my && (e = "my");
Vue.prototype.getApp = {
	_version: "2.8.9",
   platform: e,
   query: null,
   is_login: !1,
   login_complete: !1,
   is_form_id_request: !0,
	"api": require('./core/api.js'),
	"getConfig": require('./core/config.js'),
	"const": require('./core/const.js'),
	"core": require('./core/core.js'),
	"getUser": require('./core/getUser.js'),
	"login": require('./core/login.js'),
	"orderPay": require('./core/order-pay.js'),
	"page": require('./core/page.js'),
	"request": require('./core/request.js'),
	"setUser": require('./core/setUser.js'),
	"trigger": require('./core/trigger.js'),
	"helper": require('./utils/helper.js'),
	"uploader": require('./utils/uploader.js'),
	webRoot: require('./core/api.js').index.substr(0, require('./core/api.js').index.indexOf("/index.php")),
	getDappData() {
		var e = Vue.prototype.getApp,
			t = Vue.prototype.getApp.api,
			s = Vue.prototype.getApp.core;
		uni.request({
			url: t.default.dapp,
			success(t) {
				t=t.data
				0 == t.code && (s.setStorageSync(e.const.DAPP, t.data.dapp), s.setStorageSync(e.const.DAPP_NAME, t.data.dapp_name),
					s.setStorageSync(e.const.SHOW_CUSTOMER_SERVICE, t.data.show_customer_service), s.setStorageSync(e.const.CONTACT_TEL,
						t.data.contact_tel),
					s.setStorageSync(e.const.SHARE_SETTING, t.data.share_setting), e.permission_list = t.data.permission_list,
					s.setStorageSync(e.const.WXAPP_IMG, t.data.wxapp_img), s.setStorageSync(e.const.WX_BAR_TITLE, t.data.wx_bar_title),
					s.setStorageSync(e.const.ALIPAY_MP_CONFIG, t.data.alipay_mp_config), s.setStorageSync(e.const.DAPP_CONFIG, t.data),
					setTimeout(function(s) {
						e.config = t.data, e.configReadyCall && e.configReadyCall(t.data);
					}, 1e3));
			}
		})
	}
}
Vue.prototype.setData = function(obj) {
	let that = this;
	let keys = [];
	let val, data;
	Object.keys(obj).forEach(function(key) {
		keys = key.split('.');
		val = obj[key];
		data = that.$data.data;
		keys.forEach(function(key2, index) {
			if (index + 1 == keys.length) {
				that.$set(data, key2, val);
			} else {
				if (!data[key2]) {
					that.$set(data, key2, {});
				}
			}
			data = data[key2];
		})
	});
}
Vue.prototype.$yunpingLogin = function(params){
	return new Promise((resolve, reject) => {
		var that = this;
		uni.request({
			url: this.getApp.api.new.yunping_login,
		  header: {
		      "content-type": "application/x-www-form-urlencoded"
		  },
		  method: "GET",
		  dataType: "json",
			data: params,
			success: function(e){
				if(e.data.code == 1){
					that.getApp.core.reLaunch({url: "/pages/h5-login/h5-login"})
					reject()
				}else{
					that.getApp.core.setStorageSync(that.getApp.const.ACCESS_TOKEN, e.data.data.access_token),
					that.getApp.request({
						url: that.getApp.api.user.index,
						data: {
							access_token: e.data.data.access_token
						},
						success:function(e){
							that.getApp.core.setStorageSync(that.getApp.const.USER_INFO,e.data.user_info),resolve()
						}
					})
				}
			}
		})
	})
}
Vue.prototype.$yunpingLoginThird = function(params){
	return new Promise((resolve, reject) => {
		var that = this;
		uni.request({
			url: this.getApp.api.new.yunping_login_third,
		  header: {
		      "content-type": "application/x-www-form-urlencoded"
		  },
		  method: "GET",
		  dataType: "json",
			data: params,
			success: function(e){
				if(e.data.code == 1){
					that.getApp.core.reLaunch({url: "/pages/h5-login/h5-login"})
					reject()
				}else{
					that.getApp.core.setStorageSync(that.getApp.const.ACCESS_TOKEN, e.data.data.access_token),
					that.getApp.request({
						url: that.getApp.api.user.index,
						data: {
							access_token: e.data.data.access_token
						},
						success:function(e){
							that.getApp.core.setStorageSync(that.getApp.const.USER_INFO,e.data.user_info),resolve()
						}
					})
				}
			}
		})
	})
}
Vue.prototype.$webShare = function(e){
	return new Promise(function(resolve,reject){
		var root = window.location.protocol + '//' + window.location.host;
		var content = e.title,
				path = root + e.path,
				imgUrl = e.imgUrl;
		uni.showActionSheet({
			itemList: ['微信','朋友圈','QQ','QQ动态'],
			success: function(res){
					if(res.tapIndex==0){
						try{
							androidjs.shareWeChat('云商',content,path,0)
						}catch(e){
							shareWeChat('云商',content,path,0)
						}
					}else if(res.tapIndex==1){
						try{
							androidjs.shareWeChat('云商',content,path,1)
						}catch(e){
							shareWeChat('云商',content,path,1)
						}
					}else if(res.tapIndex==2){
						try{
							androidjs.shareQQ('云商',content,path,0,imgUrl)
						}catch(e){
							shareQQ('云商',content,path,0,imgUrl)
						}
					}else if(res.tapIndex==3){
						try{
							androidjs.shareQQ('云商',content,path,1,imgUrl)
						}catch(e){
							shareQQ('云商',content,path,1,imgUrl)
						}
					}
					resolve()
			}
		})
	})
}

App.mpType = 'app'

const app = new Vue({
	...App
})
app.$mount()
