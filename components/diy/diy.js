module.exports = { currentPage: null,
    gSpecificationsModel: null,
    init: function(t) {
        this.currentPage = t;
        var e = this;
        this.gSpecificationsModel = require("../../components/goods/specifications_model.js"), 
        this.gSpecificationsModel.init(t), void 0 === t.showNotice && (t.showNotice = function() {
            e.showNotice();
        }), void 0 === t.closeNotice && (t.closeNotice = function() {
            e.closeNotice();
        }), void 0 === t.play && (t.play = function(t) {
            e.play(t);
        }), void 0 === t.receive && (t.receive = function(t) {
            e.receive(t);
        }), void 0 === t.closeCouponBox && (t.closeCouponBox = function(t) {
            e.closeCouponBox(t);
        }), void 0 === t.catBind && (t.catBind = function(t) {
            e.catBind(t);
        }), void 0 === t.modalShowGoods && (t.modalShowGoods = function(t) {
            e.modalShowGoods(t);
        }), void 0 === t.modalConfirmGoods && (t.modalConfirmGoods = function(t) {
            e.modalConfirmGoods(t);
        }), void 0 === t.modalCloseGoods && (t.modalCloseGoods = function(t) {
            e.modalCloseGoods(t);
        }), void 0 === t.setTime && (t.setTime = function(t) {
            e.setTime(t);
        }), void 0 === t.closeActModal && (t.closeActModal = function(t) {
            e.closeActModal(t);
        }), void 0 === t.goto && (t.goto = function(t) {
            e.goto(t);
        }), void 0 === t.go && (t.go = function(t) {
            e.go(t);
        });
    },
    showNotice: function() {
        this.currentPage.setData({
            show_notice: !0
        });
    },
    closeNotice: function() {
        this.currentPage.setData({
            show_notice: !1
        });
    },
    play: function(t) {
        this.currentPage.setData({
            play: t.currentTarget.dataset.index
        });
    },
    receive: function(t) {
        var e = this.currentPage, a = t.currentTarget.dataset.index;
        e.getApp.core.showLoading({
            title: "领取中",
            mask: !0
        }), e.hideGetCoupon || (e.hideGetCoupon = function(t) {
            var a = t.currentTarget.dataset.url || !1;
            e.setData({
                get_coupon_list: null
            }), uni.navigateTo({
                url: a || "/pages/list/list"
            });
        }), e.getApp.request({
            url: e.getApp.api.coupon.receive,
            data: {
                id: a
            },
            success: function(t) {
                e.getApp.core.hideLoading(), 0 == t.code ? e.setData({
                    get_coupon_list: t.data.list,
                    coupon_list: t.data.coupon_list
                }) : (e.getApp.core.showToast({
                    title: t.msg,
                    duration: 2e3
                }), e.setData({
                    coupon_list: t.data.coupon_list
                }));
            }
        });
    },
    closeCouponBox: function(t) {
        this.currentPage.setData({
            get_coupon_list: ""
        });
    },
    catBind: function(t) {
        var e = this.currentPage, a = t.currentTarget.dataset.template, o = t.currentTarget.dataset.index, i = e.data.template;
        i[a].param.cat_index = o, e.setData({
            template: i
        });
    },
    modalShowGoods: function(t) {
        var e = this.currentPage, a = e.data.template, o = t.currentTarget.dataset.template, i = t.currentTarget.dataset.cat, s = t.currentTarget.dataset.goods, n = a[o].param.list[i].goods_list[s];
        "goods" == a[o].type ? (n.id = n.goods_id, e.setData({
            goods: n,
            show_attr_picker: !0,
            attr_group_list: n.attr_group_list,
            pageType: "DAPP",
            id: n.id
        }), this.gSpecificationsModel.selectDefaultAttr()) : e.getApp.core.navigateTo({
            url: n.page_url
        });
    },
    modalConfirmGoods: function(t) {
        var e = this.currentPage, a = (e.data.pageType, require("../../components/goods/goods_buy.js"));
        a.currentPage = e, a.submit("ADD_CART"), e.setData({
            form: {
                number: 1
            }
        });
    },
    modalCloseGoods: function(t) {
        this.currentPage.setData({
            show_attr_picker: !1,
            form: {
                number: 1
            }
        });
    },
    template_time: null,
    setTime: function(t) {
        var e = this, a = this.currentPage, o = a.data.time_all;
        e["template_time_" + a.data.options.page_id] && clearInterval(e["template_time_" + a.data.options.page_id]), 
        e["template_time_" + a.data.options.page_id] = setInterval(function() {
            for (var t in o) if ("time" == o[t].type && (o[t].param.start_time > 0 ? (o[t].param.start_time--, 
            o[t].param.end_time--, o[t].param.time_list = a.setTimeList(o[t].param.start_time)) : o[t].param.end_time > 0 && (o[t].param.end_time--, 
            o[t].param.time_list = a.setTimeList(o[t].param.end_time))), "miaosha" == o[t].type || "bargain" == o[t].type || "lottery" == o[t].type) {
                var e = o[t].param.cat_index;
                for (var i in o[t].param.list[e].goods_list) o[t].param.list[e].goods_list[i].time > 0 ? (o[t].param.list[e].goods_list[i].time--, 
                o[t].param.list[e].goods_list[i].time_list = a.setTimeList(o[t].param.list[e].goods_list[i].time), 
                o[t].param.list[e].goods_list[i].time_end > 0 && (o[t].param.list[e].goods_list[i].time_end--, 
                1 == o[t].param.list[e].goods_list[i].time && (o[t].param.list[e].goods_list[i].is_start = 1, 
                o[t].param.list[e].goods_list[i].time = o[t].param.list[e].goods_list[i].time_end, 
                o[t].param.list[e].goods_list[i].time_end = 0, o[t].param.list[e].goods_list[i].time_content = 1 == o[t].param.list_style ? "仅剩" : "距结束仅剩"))) : (o[t].param.list[e].goods_list[i].is_start = 1, 
                o[t].param.list[e].goods_list[i].time = 0, o[t].param.list[e].goods_list[i].time_content = "活动已结束", 
                o[t].param.list[e].goods_list[i].time_list = {});
            }
            a.setData({
                time_all: o
            });
        }, 1e3);
    },
    closeActModal: function() {
        var t, e = this.currentPage, a = e.data.act_modal_list, o = !0;
        for (var i in a) {
            var s = parseInt(i);
            a[s].show && (a[s].show = !1, void 0 !== a[t = s + 1] && o && (o = !1, setTimeout(function() {
                e.data.act_modal_list[t].show = !0, e.setData({
                    act_modal_list: e.data.act_modal_list
                });
            }, 500)));
        }
        e.setData({
            act_modal_list: a
        });
    },
    goto: function(t) {
        var e = this;
        "undefined" != typeof my ? e.location(t) : e.currentPage.getApp.core.getSetting({
            success: function(a) {
                a.authSetting["scope.userLocation"] ? e.location(t) : e.currentPage.getApp.getauth({
                    content: "需要获取您的地理位置授权，请到小程序设置中打开授权！",
                    cancel: !1,
                    author: "scope.userLocation",
                    success: function(a) {
                        console.log(a), a.authSetting["scope.userLocation"] && e.location(t);
                    }
                });
            }
        });
    },
    location: function(t) {
        var e = this.currentPage, a = [], o = t.currentTarget.dataset.template;
        a = void 0 !== o ? e.data.template[o].param.list : e.data.list;
        var i = t.currentTarget.dataset.index;
        e.getApp.core.openLocation({
            latitude: parseFloat(a[i].latitude),
            longitude: parseFloat(a[i].longitude),
            name: a[i].name,
            address: a[i].address
        });
    },
    go: function(t) {
        var e = this.currentPage, a = t.currentTarget.dataset.template, o = [];
        o = void 0 !== a ? e.data.template[a].param.list : e.data.list;
        var i = t.currentTarget.dataset.index;
        e.getApp.core.navigateTo({
            url: "/pages/shop-detail/shop-detail?shop_id=" + o[i].id
        });
    }
};