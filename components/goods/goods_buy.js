module.exports = { currentPage: null,
    init: function(t) {
        var a = this;
        a.currentPage = t, void 0 === t.favoriteAdd && (t.favoriteAdd = function(t) {
            a.favoriteAdd(t);
        }), void 0 === t.favoriteRemove && (t.favoriteRemove = function(t) {
            a.favoriteRemove(t);
        }), void 0 === t.kfMessage && (t.kfMessage = function(t) {
            a.kfMessage(t);
        }), void 0 === t.callPhone && (t.callPhone = function(t) {
            a.callPhone(t);
        }), void 0 === t.addCart && (t.addCart = function(t) {
            a.addCart(t);
        }), void 0 === t.buyNow && (t.buyNow = function(t) {
            a.buyNow(t);
        }), void 0 === t.goHome && (t.goHome = function(t) {
            a.goHome(t);
        });
    },
    favoriteAdd: function() {
        var t = this.currentPage;
        t.getApp.request({
            url: t.getApp.api.user.favorite_add,
            method: "post",
            data: {
                goods_id: t.data.goods.id
            },
            success: function(a) {
                if (0 == a.code) {
                    var e = t.data.goods;
                    e.is_favorite = 1, t.setData({
                        goods: e
                    });
                }
            }
        });
    },
    favoriteRemove: function() {
        var t = this.currentPage;
        t.getApp.request({
            url: t.getApp.api.user.favorite_remove,
            method: "post",
            data: {
                goods_id: t.data.goods.id
            },
            success: function(a) {
                if (0 == a.code) {
                    var e = t.data.goods;
                    e.is_favorite = 0, t.setData({
                        goods: e
                    });
                }
            }
        });
    },
    kfMessage: function() {
        this.currentPage.getApp.core.getStorageSync(this.currentPage.getApp.const.DAPP).show_customer_service || this.currentPage.getApp.core.showToast({
            title: "未启用客服功能"
        });
    },
    callPhone: function(t) {
        this.currentPage.getApp.core.makePhoneCall({
            phoneNumber: t.target.dataset.info
        });
    },
    addCart: function() {
        this.currentPage.data.btn && this.submit("ADD_CART");
    },
    buyNow: function() {
        this.currentPage.data.btn && this.submit("BUY_NOW");
    },
    submit: function(t) {
        var a = this.currentPage;
        if (!a.data.show_attr_picker) return a.setData({
            show_attr_picker: !0
        }), !0;
        if (a.data.miaosha_data && a.data.miaosha_data.rest_num > 0 && a.data.form.number > a.data.miaosha_data.rest_num) return this.currentPage.getApp.core.showToast({
            title: "商品库存不足，请选择其它规格或数量",
            image: "/static/images/icon-warning.png"
        }), !0;
        if (a.data.form.number > a.data.goods.num) return a.getApp.core.showToast({
            title: "商品库存不足，请选择其它规格或数量",
            image: "/static/images/icon-warning.png"
        }), !0;
        var e = a.data.attr_group_list, o = [];
        for (var r in e) {
            var i = !1;
            for (var s in e[r].attr_list) if (e[r].attr_list[s].checked) {
                i = {
                    attr_id: e[r].attr_list[s].attr_id,
                    attr_name: e[r].attr_list[s].attr_name
                };
                break;
            }
            if (!i) return a.getApp.core.showToast({
                title: "请选择" + e[r].attr_group_name,
                image: "/static/images/icon-warning.png"
            }), !0;
            o.push({
                attr_group_id: e[r].attr_group_id,
                attr_group_name: e[r].attr_group_name,
                attr_id: i.attr_id,
                attr_name: i.attr_name
            });
        }
        if ("ADD_CART" == t && (a.getApp.core.showLoading({
            title: "正在提交",
            mask: !0
        }), a.getApp.request({
            url: a.getApp.api.cart.add_cart,
            method: "POST",
            data: {
                goods_id: a.data.goods.id,
                attr: JSON.stringify(o),
                num: a.data.form.number
            },
            success: function(t) {
                a.getApp.core.hideLoading(), a.getApp.core.showToast({
                    title: t.msg,
                    duration: 1500
                }), a.setData({
                    show_attr_picker: !1
                });
            }
        })), "BUY_NOW" == t) {
            a.setData({
                show_attr_picker: !1
            });
            var n = [];
            n.push({
                goods_id: a.data.id,
                num: a.data.form.number,
                attr: o
            });
            var d = a.data.goods, g = 0;
            null != d.store && (g = d.store.id);
            var u = [];
            u.push({
                store_id: g,
                goods_list: n
            }), a.getApp.core.navigateTo({
                url: "/pages/new-order-submit/new-order-submit?store_list=" + JSON.stringify(u)
            });
        }
    },
    goHome: function(t) {
        var a = this.currentPage.data.pageType;
        if ("PINTUAN" === a) e = "/pages/pt/index/index"; else if ("BOOK" === a) e = "/pages/book/index/index"; else var e = "/pages/index/index";
        this.currentPage.getApp.core.redirectTo({
            url: e
        });
    }
};